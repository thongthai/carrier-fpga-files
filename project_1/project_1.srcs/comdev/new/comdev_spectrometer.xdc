set_property PACKAGE_PIN G14 [get_ports {SRAM_A[0]}]
set_property PACKAGE_PIN C20 [get_ports {SRAM_A[1]}]
set_property PACKAGE_PIN B20 [get_ports {SRAM_A[2]}]
set_property PACKAGE_PIN B19 [get_ports {SRAM_A[3]}]
set_property PACKAGE_PIN A20 [get_ports {SRAM_A[4]}]
set_property PACKAGE_PIN E17 [get_ports {SRAM_A[5]}]
set_property PACKAGE_PIN D18 [get_ports {SRAM_A[6]}]
set_property PACKAGE_PIN D19 [get_ports {SRAM_A[7]}]
set_property PACKAGE_PIN D20 [get_ports {SRAM_A[8]}]
set_property PACKAGE_PIN E18 [get_ports {SRAM_A[9]}]
set_property PACKAGE_PIN E19 [get_ports {SRAM_A[10]}]
set_property PACKAGE_PIN F16 [get_ports {SRAM_A[11]}]
set_property PACKAGE_PIN F17 [get_ports {SRAM_A[12]}]
set_property PACKAGE_PIN M19 [get_ports {SRAM_A[13]}]
set_property PACKAGE_PIN M20 [get_ports {SRAM_A[14]}]
set_property PACKAGE_PIN M17 [get_ports {SRAM_A[15]}]
set_property PACKAGE_PIN M18 [get_ports {SRAM_A[16]}]
set_property PACKAGE_PIN L19 [get_ports {SRAM_A[17]}]
set_property PACKAGE_PIN L20 [get_ports {SRAM_A[18]}]
set_property PACKAGE_PIN K19 [get_ports {SRAM_A[19]}]
set_property PACKAGE_PIN J19 [get_ports {SRAM_A[20]}]
set_property PACKAGE_PIN L16 [get_ports SRAM_OEN]
set_property PACKAGE_PIN L17 [get_ports SRAM_BLEN]
set_property PACKAGE_PIN K17 [get_ports SRAM_BHEN]
set_property PACKAGE_PIN K18 [get_ports SRAM_WEN]
set_property PACKAGE_PIN H16 [get_ports SRAM_CE1N]
set_property PACKAGE_PIN H17 [get_ports SRAM_CE2]
set_property PACKAGE_PIN J18 [get_ports {SRAM_DQ[0]}]
set_property PACKAGE_PIN H18 [get_ports {SRAM_DQ[1]}]
set_property PACKAGE_PIN F19 [get_ports {SRAM_DQ[2]}]
set_property PACKAGE_PIN F20 [get_ports {SRAM_DQ[3]}]
set_property PACKAGE_PIN G17 [get_ports {SRAM_DQ[4]}]
set_property PACKAGE_PIN G18 [get_ports {SRAM_DQ[5]}]
set_property PACKAGE_PIN J20 [get_ports {SRAM_DQ[6]}]
set_property PACKAGE_PIN H20 [get_ports {SRAM_DQ[7]}]
set_property PACKAGE_PIN G19 [get_ports {SRAM_DQ[8]}]
set_property PACKAGE_PIN G20 [get_ports {SRAM_DQ[9]}]
set_property PACKAGE_PIN H15 [get_ports {SRAM_DQ[10]}]
set_property PACKAGE_PIN G15 [get_ports {SRAM_DQ[11]}]
set_property PACKAGE_PIN K14 [get_ports {SRAM_DQ[12]}]
set_property PACKAGE_PIN J14 [get_ports {SRAM_DQ[13]}]
set_property PACKAGE_PIN N15 [get_ports {SRAM_DQ[14]}]
set_property PACKAGE_PIN N16 [get_ports {SRAM_DQ[15]}]

set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[31]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[30]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[29]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[28]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[27]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[26]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[25]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[24]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[23]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[22]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[21]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[20]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_A[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports adc_busy]
set_property IOSTANDARD LVCMOS33 [get_ports adc_convst]
set_property IOSTANDARD LVCMOS33 [get_ports adc_cs]
set_property IOSTANDARD LVCMOS33 [get_ports adc_rd]
set_property IOSTANDARD LVCMOS33 [get_ports adc_shdn]

set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {adc_data[0]}]

set_property IOSTANDARD LVCMOS33 [get_ports SRAM_OEN]


set_property IOSTANDARD LVCMOS33 [get_ports SRAM_BHEN]
set_property IOSTANDARD LVCMOS33 [get_ports SRAM_BLEN]

set_property IOSTANDARD LVCMOS33 [get_ports SRAM_CE2]
set_property IOSTANDARD LVCMOS33 [get_ports SRAM_CE1N]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SRAM_DQ[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports SRAM_WEN]

set_property IOSTANDARD LVCMOS33 [get_ports s00_SW_Reset]

set_property PACKAGE_PIN Y17 [get_ports adc_shdn]
set_property PACKAGE_PIN W14 [get_ports adc_rd]
set_property PACKAGE_PIN Y14 [get_ports adc_convst]
set_property PACKAGE_PIN T16 [get_ports adc_cs]
set_property PACKAGE_PIN U17 [get_ports adc_busy]

set_property PACKAGE_PIN R19 [get_ports {adc_data[0]}]
set_property PACKAGE_PIN T11 [get_ports {adc_data[1]}]
set_property PACKAGE_PIN T10 [get_ports {adc_data[2]}]
set_property PACKAGE_PIN T12 [get_ports {adc_data[3]}]
set_property PACKAGE_PIN U12 [get_ports {adc_data[4]}]
set_property PACKAGE_PIN U13 [get_ports {adc_data[5]}]
set_property PACKAGE_PIN V13 [get_ports {adc_data[6]}]
set_property PACKAGE_PIN V12 [get_ports {adc_data[7]}]
set_property PACKAGE_PIN W13 [get_ports {adc_data[8]}]
set_property PACKAGE_PIN T14 [get_ports {adc_data[9]}]
set_property PACKAGE_PIN T15 [get_ports {adc_data[10]}]
set_property PACKAGE_PIN P14 [get_ports {adc_data[11]}]
set_property PACKAGE_PIN R14 [get_ports {adc_data[12]}]
set_property PACKAGE_PIN Y16 [get_ports {adc_data[13]}]

set_property PACKAGE_PIN N18 [get_ports s01_Pwm_Tec]
set_property PACKAGE_PIN P19 [get_ports s01_Pwm_VGuard]
set_property PACKAGE_PIN N20 [get_ports s01_Pwm_VRefIn]

set_property PACKAGE_PIN U20 [get_ports LSync]
set_property PACKAGE_PIN V20 [get_ports Cap_Gain]
set_property PACKAGE_PIN Y18 [get_ports ClockL]

set_property PACKAGE_PIN T19 [get_ports s00_SW_Reset]

set_property IOSTANDARD LVCMOS33 [get_ports s01_Pwm_Tec]
set_property IOSTANDARD LVCMOS33 [get_ports s01_Pwm_VGuard]
set_property IOSTANDARD LVCMOS33 [get_ports s01_Pwm_VRefIn]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]


set_property IOSTANDARD LVCMOS33 [get_ports Cap_Gain]
set_property IOSTANDARD LVCMOS33 [get_ports ClockL]
set_property IOSTANDARD LVCMOS33 [get_ports LSync]



set_property IOSTANDARD LVCMOS33 [get_ports {sdio_0_data_io[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sdio_0_data_io[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sdio_0_data_io[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sdio_0_data_io[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports SDIO_0_clk]
set_property IOSTANDARD LVCMOS33 [get_ports sdio_0_cmd_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io0_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_io1_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_sck_io]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_ss1_o]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_ss2_o]
set_property IOSTANDARD LVCMOS33 [get_ports spi_0_ss_io]
set_property IOSTANDARD LVCMOS33 [get_ports UART_1_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports UART_1_txd]
set_property PACKAGE_PIN P16 [get_ports UART_1_rxd]
set_property PACKAGE_PIN P15 [get_ports UART_1_txd]
set_property PACKAGE_PIN V17 [get_ports sdio_0_cmd_io]
set_property PACKAGE_PIN P18 [get_ports SDIO_0_clk]
set_property PACKAGE_PIN T17 [get_ports {sdio_0_data_io[0]}]
set_property PACKAGE_PIN V18 [get_ports {sdio_0_data_io[3]}]

set_property PACKAGE_PIN V15 [get_ports {sdio_0_data_io[2]}]
set_property PACKAGE_PIN V16 [get_ports {sdio_0_data_io[1]}]

set_property IOSTANDARD LVCMOS33 [get_ports spi_clk]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cs]
set_property IOSTANDARD LVCMOS33 [get_ports spi_miso]
set_property IOSTANDARD LVCMOS33 [get_ports spi_mosi]
set_property PACKAGE_PIN L14 [get_ports spi_clk]
set_property PACKAGE_PIN L15 [get_ports spi_cs]
set_property PACKAGE_PIN M14 [get_ports spi_miso]
set_property PACKAGE_PIN M15 [get_ports spi_mosi]
