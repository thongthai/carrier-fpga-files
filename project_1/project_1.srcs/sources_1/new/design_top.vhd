----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/06/2017 01:42:26 PM
-- Design Name: 
-- Module Name: design_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity design_top is
  Port (
        FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
        FIXED_IO_ps_clk : inout STD_LOGIC;
        FIXED_IO_ps_porb : inout STD_LOGIC;
        FIXED_IO_ps_srstb : inout STD_LOGIC;
        
        SRAM_A : out STD_LOGIC_VECTOR ( 20 downto 0 );
        SRAM_DQ : inout STD_LOGIC_VECTOR ( 15 downto 0 );
        SRAM_BHEN : out std_logic;
        SRAM_BLEN : out std_logic;
        SRAM_CE1N : out std_logic;
        SRAM_CE2  : out std_logic;
        SRAM_OEN : out std_logic;
        SRAM_WEN : out STD_LOGIC;

        s00_SW_Reset : in STD_LOGIC;
        
        s01_Pwm_Tec : out STD_LOGIC;
        s01_Pwm_VGuard : out STD_LOGIC;
        s01_Pwm_VRefIn : out STD_LOGIC;
        
        
        LSync : out STD_LOGIC;
        Cap_Gain : out STD_LOGIC;
        ClockL : out STD_LOGIC;
        
        adc_cs : out STD_LOGIC;
        adc_convst : out STD_LOGIC;
        adc_rd : out STD_LOGIC;
        adc_shdn : out STD_LOGIC;
        adc_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
        adc_busy : in STD_LOGIC
      
  );
end design_top;

architecture Behavioral of design_top is
    signal SRAM_addr : std_logic_vector(31 downto 0);
    signal SRAM_ben : std_logic_vector(1 downto 0);
    signal SRAM_wait : std_logic_vector(0 downto 0);
    signal SRAM_ce : std_logic_vector(0 downto 0);
    signal SRAM_ce_n : std_logic_vector(0 downto 0);
    signal SRAM_oe_n : std_logic_vector(0 downto 0);
    
begin

SRAM_A(20 downto 0) <= SRAM_addr(20 downto 0);
SRAM_BHEN <= SRAM_ben(1);
SRAM_BLEN <= SRAM_ben(0);

SRAM_CE1N <= SRAM_ce_n(0);
SRAM_CE2 <= SRAM_ce(0);

SRAM_OEN <= SRAM_oe_n(0);

u0 : entity work.design_1_wrapper
  port map(
    FIXED_IO_mio => FIXED_IO_mio,
    FIXED_IO_ps_clk => FIXED_IO_ps_clk,
    FIXED_IO_ps_porb => FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
    
    SRAM_addr => SRAM_addr,
    SRAM_adv_ldn => open,
    SRAM_ben => SRAM_ben,
    SRAM_ce => SRAM_ce,
    SRAM_ce_n => SRAM_ce_n,
    SRAM_clken => open,
    SRAM_cre => open,
    SRAM_lbon => open,
    SRAM_oen => SRAM_oe_n,
    SRAM_qwen => open,
    SRAM_rnw => open,
    SRAM_rpn => open,
    SRAM_wen => SRAM_WEN,
    SRAM_wait => SRAM_wait,
    sram_dq_io => SRAM_DQ,
    
    SW_Reset => s00_SW_Reset,
    
    PWMTec => s01_Pwm_Tec,
    PWMVGuard => s01_Pwm_VGuard,
    PWMVRef => s01_Pwm_VRefIn,
    
    LSync => LSync,
    Cap_Gain => Cap_Gain,
    ClockL => ClockL,
    
    adc_cs => adc_cs,
    adc_convst => adc_convst,
    adc_rd => adc_rd,
    adc_shdn => adc_shdn,
    adc_data => adc_data,
    adc_busy => adc_busy
    
  );

end Behavioral;
