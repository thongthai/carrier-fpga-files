--Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
--Date        : Thu Apr 06 13:35:36 2017
--Host        : DESKTOP-GPPU8J5 running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    SRAM_addr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SRAM_adv_ldn : out STD_LOGIC;
    SRAM_ben : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SRAM_ce : out STD_LOGIC_VECTOR ( 0 to 0 );
    SRAM_ce_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    SRAM_clken : out STD_LOGIC;
    SRAM_cre : out STD_LOGIC;
    SRAM_lbon : out STD_LOGIC;
    SRAM_oen : out STD_LOGIC_VECTOR ( 0 to 0 );
    SRAM_qwen : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SRAM_rnw : out STD_LOGIC;
    SRAM_rpn : out STD_LOGIC;
    SRAM_wait : in STD_LOGIC_VECTOR ( 0 to 0 );
    SRAM_wen : out STD_LOGIC;
    VidIn : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s00_SW_Reset : in STD_LOGIC;
    s01_Pwm_Tec : out STD_LOGIC;
    s01_Pwm_VGuard : out STD_LOGIC;
    s01_Pwm_VRefIn : out STD_LOGIC;
    sram_dq_io : inout STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal sram_dq_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sram_dq_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal sram_dq_i_10 : STD_LOGIC_VECTOR ( 10 to 10 );
  signal sram_dq_i_11 : STD_LOGIC_VECTOR ( 11 to 11 );
  signal sram_dq_i_12 : STD_LOGIC_VECTOR ( 12 to 12 );
  signal sram_dq_i_13 : STD_LOGIC_VECTOR ( 13 to 13 );
  signal sram_dq_i_14 : STD_LOGIC_VECTOR ( 14 to 14 );
  signal sram_dq_i_15 : STD_LOGIC_VECTOR ( 15 to 15 );
  signal sram_dq_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal sram_dq_i_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal sram_dq_i_4 : STD_LOGIC_VECTOR ( 4 to 4 );
  signal sram_dq_i_5 : STD_LOGIC_VECTOR ( 5 to 5 );
  signal sram_dq_i_6 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal sram_dq_i_7 : STD_LOGIC_VECTOR ( 7 to 7 );
  signal sram_dq_i_8 : STD_LOGIC_VECTOR ( 8 to 8 );
  signal sram_dq_i_9 : STD_LOGIC_VECTOR ( 9 to 9 );
  signal sram_dq_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sram_dq_io_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal sram_dq_io_10 : STD_LOGIC_VECTOR ( 10 to 10 );
  signal sram_dq_io_11 : STD_LOGIC_VECTOR ( 11 to 11 );
  signal sram_dq_io_12 : STD_LOGIC_VECTOR ( 12 to 12 );
  signal sram_dq_io_13 : STD_LOGIC_VECTOR ( 13 to 13 );
  signal sram_dq_io_14 : STD_LOGIC_VECTOR ( 14 to 14 );
  signal sram_dq_io_15 : STD_LOGIC_VECTOR ( 15 to 15 );
  signal sram_dq_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal sram_dq_io_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal sram_dq_io_4 : STD_LOGIC_VECTOR ( 4 to 4 );
  signal sram_dq_io_5 : STD_LOGIC_VECTOR ( 5 to 5 );
  signal sram_dq_io_6 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal sram_dq_io_7 : STD_LOGIC_VECTOR ( 7 to 7 );
  signal sram_dq_io_8 : STD_LOGIC_VECTOR ( 8 to 8 );
  signal sram_dq_io_9 : STD_LOGIC_VECTOR ( 9 to 9 );
  signal sram_dq_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sram_dq_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal sram_dq_o_10 : STD_LOGIC_VECTOR ( 10 to 10 );
  signal sram_dq_o_11 : STD_LOGIC_VECTOR ( 11 to 11 );
  signal sram_dq_o_12 : STD_LOGIC_VECTOR ( 12 to 12 );
  signal sram_dq_o_13 : STD_LOGIC_VECTOR ( 13 to 13 );
  signal sram_dq_o_14 : STD_LOGIC_VECTOR ( 14 to 14 );
  signal sram_dq_o_15 : STD_LOGIC_VECTOR ( 15 to 15 );
  signal sram_dq_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal sram_dq_o_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal sram_dq_o_4 : STD_LOGIC_VECTOR ( 4 to 4 );
  signal sram_dq_o_5 : STD_LOGIC_VECTOR ( 5 to 5 );
  signal sram_dq_o_6 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal sram_dq_o_7 : STD_LOGIC_VECTOR ( 7 to 7 );
  signal sram_dq_o_8 : STD_LOGIC_VECTOR ( 8 to 8 );
  signal sram_dq_o_9 : STD_LOGIC_VECTOR ( 9 to 9 );
  signal sram_dq_t_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sram_dq_t_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal sram_dq_t_10 : STD_LOGIC_VECTOR ( 10 to 10 );
  signal sram_dq_t_11 : STD_LOGIC_VECTOR ( 11 to 11 );
  signal sram_dq_t_12 : STD_LOGIC_VECTOR ( 12 to 12 );
  signal sram_dq_t_13 : STD_LOGIC_VECTOR ( 13 to 13 );
  signal sram_dq_t_14 : STD_LOGIC_VECTOR ( 14 to 14 );
  signal sram_dq_t_15 : STD_LOGIC_VECTOR ( 15 to 15 );
  signal sram_dq_t_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal sram_dq_t_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal sram_dq_t_4 : STD_LOGIC_VECTOR ( 4 to 4 );
  signal sram_dq_t_5 : STD_LOGIC_VECTOR ( 5 to 5 );
  signal sram_dq_t_6 : STD_LOGIC_VECTOR ( 6 to 6 );
  signal sram_dq_t_7 : STD_LOGIC_VECTOR ( 7 to 7 );
  signal sram_dq_t_8 : STD_LOGIC_VECTOR ( 8 to 8 );
  signal sram_dq_t_9 : STD_LOGIC_VECTOR ( 9 to 9 );
begin
design_1_i: entity work.design_1
     port map (
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      SRAM_addr(31 downto 0) => SRAM_addr(31 downto 0),
      SRAM_adv_ldn => SRAM_adv_ldn,
      SRAM_ben(1 downto 0) => SRAM_ben(1 downto 0),
      SRAM_ce(0) => SRAM_ce(0),
      SRAM_ce_n(0) => SRAM_ce_n(0),
      SRAM_clken => SRAM_clken,
      SRAM_cre => SRAM_cre,
      SRAM_dq_i(15) => sram_dq_i_15(15),
      SRAM_dq_i(14) => sram_dq_i_14(14),
      SRAM_dq_i(13) => sram_dq_i_13(13),
      SRAM_dq_i(12) => sram_dq_i_12(12),
      SRAM_dq_i(11) => sram_dq_i_11(11),
      SRAM_dq_i(10) => sram_dq_i_10(10),
      SRAM_dq_i(9) => sram_dq_i_9(9),
      SRAM_dq_i(8) => sram_dq_i_8(8),
      SRAM_dq_i(7) => sram_dq_i_7(7),
      SRAM_dq_i(6) => sram_dq_i_6(6),
      SRAM_dq_i(5) => sram_dq_i_5(5),
      SRAM_dq_i(4) => sram_dq_i_4(4),
      SRAM_dq_i(3) => sram_dq_i_3(3),
      SRAM_dq_i(2) => sram_dq_i_2(2),
      SRAM_dq_i(1) => sram_dq_i_1(1),
      SRAM_dq_i(0) => sram_dq_i_0(0),
      SRAM_dq_o(15) => sram_dq_o_15(15),
      SRAM_dq_o(14) => sram_dq_o_14(14),
      SRAM_dq_o(13) => sram_dq_o_13(13),
      SRAM_dq_o(12) => sram_dq_o_12(12),
      SRAM_dq_o(11) => sram_dq_o_11(11),
      SRAM_dq_o(10) => sram_dq_o_10(10),
      SRAM_dq_o(9) => sram_dq_o_9(9),
      SRAM_dq_o(8) => sram_dq_o_8(8),
      SRAM_dq_o(7) => sram_dq_o_7(7),
      SRAM_dq_o(6) => sram_dq_o_6(6),
      SRAM_dq_o(5) => sram_dq_o_5(5),
      SRAM_dq_o(4) => sram_dq_o_4(4),
      SRAM_dq_o(3) => sram_dq_o_3(3),
      SRAM_dq_o(2) => sram_dq_o_2(2),
      SRAM_dq_o(1) => sram_dq_o_1(1),
      SRAM_dq_o(0) => sram_dq_o_0(0),
      SRAM_dq_t(15) => sram_dq_t_15(15),
      SRAM_dq_t(14) => sram_dq_t_14(14),
      SRAM_dq_t(13) => sram_dq_t_13(13),
      SRAM_dq_t(12) => sram_dq_t_12(12),
      SRAM_dq_t(11) => sram_dq_t_11(11),
      SRAM_dq_t(10) => sram_dq_t_10(10),
      SRAM_dq_t(9) => sram_dq_t_9(9),
      SRAM_dq_t(8) => sram_dq_t_8(8),
      SRAM_dq_t(7) => sram_dq_t_7(7),
      SRAM_dq_t(6) => sram_dq_t_6(6),
      SRAM_dq_t(5) => sram_dq_t_5(5),
      SRAM_dq_t(4) => sram_dq_t_4(4),
      SRAM_dq_t(3) => sram_dq_t_3(3),
      SRAM_dq_t(2) => sram_dq_t_2(2),
      SRAM_dq_t(1) => sram_dq_t_1(1),
      SRAM_dq_t(0) => sram_dq_t_0(0),
      SRAM_lbon => SRAM_lbon,
      SRAM_oen(0) => SRAM_oen(0),
      SRAM_qwen(1 downto 0) => SRAM_qwen(1 downto 0),
      SRAM_rnw => SRAM_rnw,
      SRAM_rpn => SRAM_rpn,
      SRAM_wait(0) => SRAM_wait(0),
      SRAM_wen => SRAM_wen,
      VidIn(13 downto 0) => VidIn(13 downto 0),
      s00_SW_Reset => s00_SW_Reset,
      s01_Pwm_Tec => s01_Pwm_Tec,
      s01_Pwm_VGuard => s01_Pwm_VGuard,
      s01_Pwm_VRefIn => s01_Pwm_VRefIn
    );
sram_dq_iobuf_0: component IOBUF
     port map (
      I => sram_dq_o_0(0),
      IO => sram_dq_io(0),
      O => sram_dq_i_0(0),
      T => sram_dq_t_0(0)
    );
sram_dq_iobuf_1: component IOBUF
     port map (
      I => sram_dq_o_1(1),
      IO => sram_dq_io(1),
      O => sram_dq_i_1(1),
      T => sram_dq_t_1(1)
    );
sram_dq_iobuf_10: component IOBUF
     port map (
      I => sram_dq_o_10(10),
      IO => sram_dq_io(10),
      O => sram_dq_i_10(10),
      T => sram_dq_t_10(10)
    );
sram_dq_iobuf_11: component IOBUF
     port map (
      I => sram_dq_o_11(11),
      IO => sram_dq_io(11),
      O => sram_dq_i_11(11),
      T => sram_dq_t_11(11)
    );
sram_dq_iobuf_12: component IOBUF
     port map (
      I => sram_dq_o_12(12),
      IO => sram_dq_io(12),
      O => sram_dq_i_12(12),
      T => sram_dq_t_12(12)
    );
sram_dq_iobuf_13: component IOBUF
     port map (
      I => sram_dq_o_13(13),
      IO => sram_dq_io(13),
      O => sram_dq_i_13(13),
      T => sram_dq_t_13(13)
    );
sram_dq_iobuf_14: component IOBUF
     port map (
      I => sram_dq_o_14(14),
      IO => sram_dq_io(14),
      O => sram_dq_i_14(14),
      T => sram_dq_t_14(14)
    );
sram_dq_iobuf_15: component IOBUF
     port map (
      I => sram_dq_o_15(15),
      IO => sram_dq_io(15),
      O => sram_dq_i_15(15),
      T => sram_dq_t_15(15)
    );
sram_dq_iobuf_2: component IOBUF
     port map (
      I => sram_dq_o_2(2),
      IO => sram_dq_io(2),
      O => sram_dq_i_2(2),
      T => sram_dq_t_2(2)
    );
sram_dq_iobuf_3: component IOBUF
     port map (
      I => sram_dq_o_3(3),
      IO => sram_dq_io(3),
      O => sram_dq_i_3(3),
      T => sram_dq_t_3(3)
    );
sram_dq_iobuf_4: component IOBUF
     port map (
      I => sram_dq_o_4(4),
      IO => sram_dq_io(4),
      O => sram_dq_i_4(4),
      T => sram_dq_t_4(4)
    );
sram_dq_iobuf_5: component IOBUF
     port map (
      I => sram_dq_o_5(5),
      IO => sram_dq_io(5),
      O => sram_dq_i_5(5),
      T => sram_dq_t_5(5)
    );
sram_dq_iobuf_6: component IOBUF
     port map (
      I => sram_dq_o_6(6),
      IO => sram_dq_io(6),
      O => sram_dq_i_6(6),
      T => sram_dq_t_6(6)
    );
sram_dq_iobuf_7: component IOBUF
     port map (
      I => sram_dq_o_7(7),
      IO => sram_dq_io(7),
      O => sram_dq_i_7(7),
      T => sram_dq_t_7(7)
    );
sram_dq_iobuf_8: component IOBUF
     port map (
      I => sram_dq_o_8(8),
      IO => sram_dq_io(8),
      O => sram_dq_i_8(8),
      T => sram_dq_t_8(8)
    );
sram_dq_iobuf_9: component IOBUF
     port map (
      I => sram_dq_o_9(9),
      IO => sram_dq_io(9),
      O => sram_dq_i_9(9),
      T => sram_dq_t_9(9)
    );
end STRUCTURE;
