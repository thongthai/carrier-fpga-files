connect -url tcp:127.0.0.1:3121
source C:/comdev/carrier-fpga-files/project_1/project_1.sdk/design_top_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Platform Cable USB II 000018cd3c0e01"} -index 0
loadhw C:/comdev/carrier-fpga-files/project_1/project_1.sdk/design_top_hw_platform_0/system.hdf
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Platform Cable USB II 000018cd3c0e01"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Platform Cable USB II 000018cd3c0e01"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Platform Cable USB II 000018cd3c0e01"} -index 0
dow C:/comdev/carrier-firmware/carrier-app/Debug/carrier-app.elf
bpadd -addr &main
