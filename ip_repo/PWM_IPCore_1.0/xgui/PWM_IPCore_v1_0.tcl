# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set C_S01_AXI_DATA_WIDTH [ipgui::add_param $IPINST -name "C_S01_AXI_DATA_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Width of S_AXI data bus} ${C_S01_AXI_DATA_WIDTH}
  set C_S01_AXI_ADDR_WIDTH [ipgui::add_param $IPINST -name "C_S01_AXI_ADDR_WIDTH" -parent ${Page_0}]
  set_property tooltip {Width of S_AXI address bus} ${C_S01_AXI_ADDR_WIDTH}
  ipgui::add_param $IPINST -name "C_S01_AXI_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S01_AXI_HIGHADDR" -parent ${Page_0}

  ipgui::add_param $IPINST -name "C_S01_NVGuardIn"
  ipgui::add_param $IPINST -name "C_S01_NvRefIn"
  ipgui::add_param $IPINST -name "C_S01_TecCtrlLoopEn"
  ipgui::add_param $IPINST -name "C_S01_NSetTEC"

}

proc update_PARAM_VALUE.C_S01_NSetTEC { PARAM_VALUE.C_S01_NSetTEC } {
	# Procedure called to update C_S01_NSetTEC when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_NSetTEC { PARAM_VALUE.C_S01_NSetTEC } {
	# Procedure called to validate C_S01_NSetTEC
	return true
}

proc update_PARAM_VALUE.C_S01_NVGuardIn { PARAM_VALUE.C_S01_NVGuardIn } {
	# Procedure called to update C_S01_NVGuardIn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_NVGuardIn { PARAM_VALUE.C_S01_NVGuardIn } {
	# Procedure called to validate C_S01_NVGuardIn
	return true
}

proc update_PARAM_VALUE.C_S01_NvRefIn { PARAM_VALUE.C_S01_NvRefIn } {
	# Procedure called to update C_S01_NvRefIn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_NvRefIn { PARAM_VALUE.C_S01_NvRefIn } {
	# Procedure called to validate C_S01_NvRefIn
	return true
}

proc update_PARAM_VALUE.C_S01_TecCtrlLoopEn { PARAM_VALUE.C_S01_TecCtrlLoopEn } {
	# Procedure called to update C_S01_TecCtrlLoopEn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_TecCtrlLoopEn { PARAM_VALUE.C_S01_TecCtrlLoopEn } {
	# Procedure called to validate C_S01_TecCtrlLoopEn
	return true
}

proc update_PARAM_VALUE.C_S01_AXI_DATA_WIDTH { PARAM_VALUE.C_S01_AXI_DATA_WIDTH } {
	# Procedure called to update C_S01_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_AXI_DATA_WIDTH { PARAM_VALUE.C_S01_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S01_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S01_AXI_ADDR_WIDTH { PARAM_VALUE.C_S01_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S01_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_AXI_ADDR_WIDTH { PARAM_VALUE.C_S01_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S01_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S01_AXI_BASEADDR { PARAM_VALUE.C_S01_AXI_BASEADDR } {
	# Procedure called to update C_S01_AXI_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_AXI_BASEADDR { PARAM_VALUE.C_S01_AXI_BASEADDR } {
	# Procedure called to validate C_S01_AXI_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S01_AXI_HIGHADDR { PARAM_VALUE.C_S01_AXI_HIGHADDR } {
	# Procedure called to update C_S01_AXI_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S01_AXI_HIGHADDR { PARAM_VALUE.C_S01_AXI_HIGHADDR } {
	# Procedure called to validate C_S01_AXI_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S01_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S01_AXI_DATA_WIDTH PARAM_VALUE.C_S01_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S01_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S01_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S01_AXI_ADDR_WIDTH PARAM_VALUE.C_S01_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S01_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S01_NVGuardIn { MODELPARAM_VALUE.C_S01_NVGuardIn PARAM_VALUE.C_S01_NVGuardIn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_NVGuardIn}] ${MODELPARAM_VALUE.C_S01_NVGuardIn}
}

proc update_MODELPARAM_VALUE.C_S01_NvRefIn { MODELPARAM_VALUE.C_S01_NvRefIn PARAM_VALUE.C_S01_NvRefIn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_NvRefIn}] ${MODELPARAM_VALUE.C_S01_NvRefIn}
}

proc update_MODELPARAM_VALUE.C_S01_NSetTEC { MODELPARAM_VALUE.C_S01_NSetTEC PARAM_VALUE.C_S01_NSetTEC } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_NSetTEC}] ${MODELPARAM_VALUE.C_S01_NSetTEC}
}

proc update_MODELPARAM_VALUE.C_S01_TecCtrlLoopEn { MODELPARAM_VALUE.C_S01_TecCtrlLoopEn PARAM_VALUE.C_S01_TecCtrlLoopEn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S01_TecCtrlLoopEn}] ${MODELPARAM_VALUE.C_S01_TecCtrlLoopEn}
}

