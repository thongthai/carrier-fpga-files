-- Pwm.vhd
--****************************************************************************
--    Copyright(c) 2010 COM DEV Canada. All Rights Reserved.
--****************************************************************************
--  TEMPLATE NUMBER     : 
--  PROJECT NAME        : SWSH
--  MODULE NAME         : Pwm.vhd
--  MODULE AUTHOR       : Rodney Norman  
--  DATE CREATED        : 2010-December-18
--  MODULE PART NUMBER  : 
--  MODULE VERSION      : v00.01
--  MODULE LANGUAGE     : VHDL-93
-- +--------+--------+-----------+--------------------------------------------
-- |VERSION |AUTHOR  |DATE       |DESCRIPTION OF CHANGE
-- +--------+--------+-----------+--------------------------------------------
--  V00.02     RN     2010-12-18 Created. 
--============================================================================
library IEEE;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_STD.all;

--use work.Swish_Pkg1.All;

--**************************************************************************** 
-- Entity declaration section start
--============================================================================
entity Pwm is
port   (
i_Clk24              :in    std_logic;
i_Rst24              :in    std_logic;
i_Tick1us            :in    std_logic;

--i_Cmnd_Reg          : In    Swish_Cmnd_Reg_Type;
i_NVGuardIn           : in    std_logic_vector(15 downto 0);
i_NvRefIn           : in    std_logic_vector(15 downto 0);
i_TecCtrlLoopEn     : in    std_logic;
i_NSetTEC           : in    std_logic_vector(15 downto 0);

-- i_Cmnd_Reg.NVGuardIn
-- i_Cmnd_Reg.NVRefIn 
-- i_Cmnd_Reg.TecCtrlLoopEn
-- i_Cmnd_Reg.NSetTEC


i_LoopToTec         :in    std_logic_vector(15 downto 0);

o_Pwm_VRefIn         :out   std_logic;
o_Pwm_VGuard         :out   std_logic;
o_Pwm_Tec            :out   std_logic
);
end entity Pwm;

--============================================================================
architecture RTL of Pwm is
--============================================================================
constant K0_16B               : std_logic_vector(15 downto 0) := (others => '0');
Signal s_Pwm_Cnt              : std_logic_vector(15 downto 0);
Signal s_NPwm_Tec             : std_logic_vector(15 downto 0);

--============================================================================
Begin 
--============================================================================

--============================================================================
Pwm_Cnt: Process (i_Rst24, i_Clk24)
--============================================================================
Begin
  if(i_Rst24 = '1')
  then s_Pwm_Cnt <= K0_16B;
  elsif (rising_edge(i_Clk24)) 
  then 
    If i_Tick1us = '1'
    Then s_Pwm_Cnt <= s_Pwm_Cnt + 1;
    End If;
  end if;  
end Process;  

--============================================================================
Pwm: Process (i_Rst24, i_Clk24)
--============================================================================
Begin 
If(i_Rst24 = '1')  
Then 
  o_Pwm_VRefIn  <= '0';
  o_Pwm_VGuard  <= '0';
  o_Pwm_Tec     <= '0';
elsif (rising_edge(i_Clk24))
Then
  If i_Tick1us = '1'
  Then
    If s_Pwm_Cnt >= i_NVGuardIn
    Then o_Pwm_VGuard  <= '0'; 
    Else o_Pwm_VGuard  <= '1'; 
    End If;
  
    If s_Pwm_Cnt >= i_NVRefIn 
    Then o_Pwm_VRefIn  <= '0'; 
    Else o_Pwm_VRefIn  <= '1'; 
    End If;

    If s_Pwm_Cnt >= s_NPwm_Tec
    Then o_Pwm_Tec     <= '0'; 
    Else o_Pwm_Tec     <= '1'; 
    End If;
  End If;
End if;  
End Process;  

--============================================================================
Pwm_Tec_Src: Process ( i_TecCtrlLoopEn, i_LoopToTec, i_NSetTEC )
--============================================================================
Begin 
If i_TecCtrlLoopEn = '1'
Then s_NPwm_Tec <= i_LoopToTec;
Else s_NPwm_Tec <= i_NSetTEC;
End If;
End Process;  

end architecture Rtl;