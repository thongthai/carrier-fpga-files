-- Tec_Ctrl_Loop.vhd

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--    Copyright(c) 2010 COM DEV Canada. All Rights Reserved.
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  TEMPLATE NUMBER     : 
--  PROJECT NAME        : SWSH
--  MODULE NAME         : M2_3_Tec_Ctrl_Loop
--  MODULE AUTHOR       : Rodney Norman  
--  DATE CREATED        : 2011-05-17
--  MODULE PART NUMBER  : 
--  MODULE VERSION      : v00.01
--  MODULE LANGUAGE     : VHDL-93
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- |VERSION |AUTHOR  |DATE       |DESCRIPTION OF CHANGE
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  V00.01     RN     2011-05-17
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--
-- Prefixes (Start lower case): 
-- i_    = Inputs, 
-- o_    = Outputs, 
-- io_   = Input/outputs
-- s_    = Signal
-- c_    = Combinatorial Signal
-- r_    = Registered Signal
-- k     = Constant
-- m_    = (Algorithmic) State Machine "State" signal
-- a_    = (Algorithmic) State Machine "State" (enumerated type)
-- t_    = Type
-- p_    = Process
-- v_    = variable
--
-- ?v_   = ? replaced by above letters, v = multi bit vector
--
-- Suffixes 
-- _Z1, _Z2 in general indicate the original signal pipeline delay by the indicated number of clock periods.
-- _n    = Active low
--
--
library IEEE;
--use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_1164.all;
--use work.Swish_Pkg1.All;
use IEEE.numeric_STD.all;

--use IEEE.std_logic_textio.all;
--use std.textio.all;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
entity Tec_Ctrl_Loop is
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
port   (
i_CLK24M              : in    std_logic;
i_Rst24M              : in    std_logic;

--i_CtrlReg             : In Swish_Cmnd_Reg_Type;
i_TecCtrlLoopEn       : in std_logic;
i_NSetTEC             : in std_logic_vector(15 downto 0);
i_TCLGain             : in std_logic_vector(3 downto 0);
i_NSetTemperature     : in std_logic_vector(15 downto 0);
i_TCLSign             : in std_logic;

i_HKAdcAdd            : in   std_logic_vector(3 downto 0);
i_HKAdcWe             : in   std_logic;
i_HKAdcDt             : in   std_logic_vector(15 downto 0);

o_LoopToTec           : out   std_logic_vector(15 downto 0)

);
end entity Tec_Ctrl_Loop;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
architecture RTL of Tec_Ctrl_Loop is
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

Constant k0_16b        : std_logic_vector(15 downto 0) := (Others => '0');
Constant k0_32b        : std_logic_vector(31 downto 0) := (Others => '0');

Signal s_TecLoopInDt     : std_logic_vector(15 downto 0);
Signal s_dTemptur_17b  : std_logic_vector(16 downto 0);
Signal s_dTemptur_47b  : std_logic_vector(46 downto 0);
Signal s_ShiftOut      : std_logic_vector(31 downto 0);
Signal s_AccumOut      : std_logic_vector(31 downto 0);
Signal s_Sum           : std_logic_vector(32 downto 0);
Signal s_Saturated     : std_logic_vector(31 downto 0);
Signal s_AccumIn       : std_logic_vector(31 downto 0);

Signal s_TecLoopWe     : std_logic;
Signal s_TecLoopWeZ1   : std_logic;
Signal s_TecLoopWeZ2   : std_logic;

Signal s_TecCtrlLoopEn        : std_logic;
Signal s_NSetTEC              : std_logic_vector(15 downto 0);
Signal s_TCLGain              : std_logic_vector(3 downto 0);
Signal s_NSetTemperature      : std_logic_vector(15 downto 0);
Signal s_TCLSign              : std_logic;


-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
begin
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- This module consists of 8 blocks as shown and numbered in fig tbd of tbd.
-- The block 1 registers inputs and delays an output register enable.
-- Blocks 2 through 8 is a chain of functions. The output of each feed the input of the next.
-- The chain consists of
-- Block 2 -- Temperature error (Difference) calculation. The difference can be calculated as 
--            Measured-Set or Set-Measured in accordance with the i_CtrlReg.TCLSign bit so 
--            negitive feedback can be selected irrespective of whether a negative or positive
--            temperature measurement characteristic is available.
-- Block 3 -- Sign extension
-- Block 4 -- Loop gain control by shift function
-- Block 5 -- Accumulation Sum
-- Block 6 -- Sum over/underflow saturation
-- Block 7 -- Loop En selects: loop or i_CtrlReg.NSetTEC data
-- Block 8 -- Accumulation/Output register

s_TecCtrlLoopEn        <= i_TecCtrlLoopEn;
s_NSetTEC              <= i_NSetTEC;
s_TCLGain              <= i_TCLGain;
s_NSetTemperature      <= i_NSetTemperature;
s_TCLSign              <= i_TCLSign;

--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 1, Fig tbd 
p_InputRg : Process (i_Rst24M, i_CLK24M, i_HKAdcAdd, i_HKAdcWe) 
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
s_TecLoopWe <= '0';
If (i_HKAdcAdd = "1100") And (i_HKAdcWe = '1') Then s_TecLoopWe <= '1'; End If; 

If (i_Rst24M = '1') 
Then  
  s_TecLoopInDt <= k0_16b;
  s_TecLoopWeZ1 <= '0';
  s_TecLoopWeZ2 <= '0';
Elsif (rising_edge(i_CLK24M)) 
Then
  s_TecLoopWeZ2 <= s_TecLoopWeZ1;
  s_TecLoopWeZ1 <= s_TecLoopWe;
  If s_TecLoopWe= '1' Then s_TecLoopInDt <= i_HKAdcDt; End if;  
End if;  
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 2, Fig tbd 
p_dTemptur : Process (s_NSetTemperature, s_TecLoopInDt, s_TCLSign) 
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If s_TCLSign = '1'
Then s_dTemptur_17b <= ('0' & s_TecLoopInDt) - ('0' & s_NSetTemperature);
Else s_dTemptur_17b <= ('0' & s_NSetTemperature) - ('0' & s_TecLoopInDt);
End If;
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~


--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 3, Fig tbd 
p_SignExtend : Process (s_dTemptur_17b)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
s_dTemptur_47b(15 downto 0) <= k0_16b;
s_dTemptur_47b(31 downto 16) <= s_dTemptur_17b(15 downto 0);
If s_dTemptur_17b(16) = '1'
Then s_dTemptur_47b(46 downto 32) <= "111" & x"FFF";
Else s_dTemptur_47b(46 downto 32) <= "000" & x"000";
End If;
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~


--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 4, Fig tbd 
p_ShiftReg : Process (s_dTemptur_47b, s_TCLGain)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- s_ShiftOut <= 32 bits of s_dTemptur_47b Shr (15-TCLGain)
Begin 
Case s_TCLGain is
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
When "0000" => s_ShiftOut <= s_dTemptur_47b(46 downto 15);
When "0001" => s_ShiftOut <= s_dTemptur_47b(45 downto 14);
When "0010" => s_ShiftOut <= s_dTemptur_47b(44 downto 13);
When "0011" => s_ShiftOut <= s_dTemptur_47b(43 downto 12);

When "0100" => s_ShiftOut <= s_dTemptur_47b(42 downto 11);
When "0101" => s_ShiftOut <= s_dTemptur_47b(41 downto 10);
When "0110" => s_ShiftOut <= s_dTemptur_47b(40 downto  9);
When "0111" => s_ShiftOut <= s_dTemptur_47b(39 downto  8);

When "1000" => s_ShiftOut <= s_dTemptur_47b(38 downto  7);
When "1001" => s_ShiftOut <= s_dTemptur_47b(37 downto  6);
When "1010" => s_ShiftOut <= s_dTemptur_47b(36 downto  5);
When "1011" => s_ShiftOut <= s_dTemptur_47b(35 downto  4);

When "1100" => s_ShiftOut <= s_dTemptur_47b(34 downto  3);
When "1101" => s_ShiftOut <= s_dTemptur_47b(33 downto  2);
When "1110" => s_ShiftOut <= s_dTemptur_47b(32 downto  1);
When "1111" => s_ShiftOut <= s_dTemptur_47b(31 downto  0);
When Others => Null;
End Case;
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~


--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 5, 6 & 7 Fig tbd 
p_Sum_Sat_Sel : Process (s_ShiftOut, s_AccumOut, s_Sum, 
                     s_TecCtrlLoopEn, s_NSetTEC,
                     s_Saturated ) 
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
s_Sum <= (s_ShiftOut(31) & s_ShiftOut) + ('0' & s_AccumOut);

Case s_Sum(32 downto 31) is
When "00" => s_Saturated <= s_Sum(31 downto 0);
When "01" => s_Saturated <= s_Sum(31 downto 0);
When "10" => s_Saturated <= x"FFFFFFFF";
When "11" => s_Saturated <= x"00000000";
When Others => Null;
End Case;

If s_TecCtrlLoopEn = '1'
Then s_AccumIn <= s_Saturated;
Else s_AccumIn <= s_NSetTEC & x"0000";
End If;
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~


--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~             -- Block 8, Fig tbd 
p_AccumOut : Process (i_Rst24M, i_CLK24M, s_AccumOut(31 downto 16), s_NSetTEC) 
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then  
  s_AccumOut <= s_NSetTEC & x"0000";
Elsif (rising_edge(i_CLK24M)) 
And ((s_TecLoopWeZ2 = '1') Or (s_TecCtrlLoopEn = '0'))
Then s_AccumOut <= s_AccumIn;
End If;

o_LoopToTec <= s_AccumOut(31 downto 16);
End Process;  
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~~~
end architecture RTL;
--  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~~~
