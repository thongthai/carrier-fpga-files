library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Tec_IPCore_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S03_AXI
		C_S03_AXI_DATA_WIDTH	: integer	:= 32;
		C_S03_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
        CLK24M              : in    std_logic;
        Rst24M              : in    std_logic;
        
        LoopToTec         : out   std_logic_vector(15 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S03_AXI
		s03_axi_aclk	: in std_logic;
		s03_axi_aresetn	: in std_logic;
		s03_axi_awaddr	: in std_logic_vector(C_S03_AXI_ADDR_WIDTH-1 downto 0);
		s03_axi_awprot	: in std_logic_vector(2 downto 0);
		s03_axi_awvalid	: in std_logic;
		s03_axi_awready	: out std_logic;
		s03_axi_wdata	: in std_logic_vector(C_S03_AXI_DATA_WIDTH-1 downto 0);
		s03_axi_wstrb	: in std_logic_vector((C_S03_AXI_DATA_WIDTH/8)-1 downto 0);
		s03_axi_wvalid	: in std_logic;
		s03_axi_wready	: out std_logic;
		s03_axi_bresp	: out std_logic_vector(1 downto 0);
		s03_axi_bvalid	: out std_logic;
		s03_axi_bready	: in std_logic;
		s03_axi_araddr	: in std_logic_vector(C_S03_AXI_ADDR_WIDTH-1 downto 0);
		s03_axi_arprot	: in std_logic_vector(2 downto 0);
		s03_axi_arvalid	: in std_logic;
		s03_axi_arready	: out std_logic;
		s03_axi_rdata	: out std_logic_vector(C_S03_AXI_DATA_WIDTH-1 downto 0);
		s03_axi_rresp	: out std_logic_vector(1 downto 0);
		s03_axi_rvalid	: out std_logic;
		s03_axi_rready	: in std_logic
	);
end Tec_IPCore_v1_0;

architecture arch_imp of Tec_IPCore_v1_0 is

	-- component declaration
	component Tec_IPCore_v1_0_S03_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 5
		);
		port (
		S_CLK24M              : In    std_logic;
        S_Rst24M              : In    std_logic;
        S_HKAdcAdd            : In   std_logic_vector(3 downto 0);
        S_HKAdcWe             : In   std_logic;
        S_HKAdcDt             : In   std_logic_vector(15 downto 0);
        
        S_LoopToTec           : Out   std_logic_vector(15 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component Tec_IPCore_v1_0_S03_AXI;

begin

-- Instantiation of Axi Bus Interface S03_AXI
Tec_IPCore_v1_0_S03_AXI_inst : Tec_IPCore_v1_0_S03_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S03_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S03_AXI_ADDR_WIDTH
	)
	port map (
        S_CLK24M  => CLK24M,
        S_Rst24M  => Rst24M,
        S_HKAdcAdd => x"0",
        S_HKAdcWe => '0',
        S_HKAdcDt => x"0000",
        S_LoopToTec => LoopToTec,
		S_AXI_ACLK	=> s03_axi_aclk,
		S_AXI_ARESETN	=> s03_axi_aresetn,
		S_AXI_AWADDR	=> s03_axi_awaddr,
		S_AXI_AWPROT	=> s03_axi_awprot,
		S_AXI_AWVALID	=> s03_axi_awvalid,
		S_AXI_AWREADY	=> s03_axi_awready,
		S_AXI_WDATA	=> s03_axi_wdata,
		S_AXI_WSTRB	=> s03_axi_wstrb,
		S_AXI_WVALID	=> s03_axi_wvalid,
		S_AXI_WREADY	=> s03_axi_wready,
		S_AXI_BRESP	=> s03_axi_bresp,
		S_AXI_BVALID	=> s03_axi_bvalid,
		S_AXI_BREADY	=> s03_axi_bready,
		S_AXI_ARADDR	=> s03_axi_araddr,
		S_AXI_ARPROT	=> s03_axi_arprot,
		S_AXI_ARVALID	=> s03_axi_arvalid,
		S_AXI_ARREADY	=> s03_axi_arready,
		S_AXI_RDATA	=> s03_axi_rdata,
		S_AXI_RRESP	=> s03_axi_rresp,
		S_AXI_RVALID	=> s03_axi_rvalid,
		S_AXI_RREADY	=> s03_axi_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
