library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LTC1419_AXIS_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 4;

		-- Parameters of Axi Master Bus Interface AXIS
		C_AXIS_TDATA_WIDTH	: integer	:= 32;
		C_AXIS_START_COUNT	: integer	:= 32
	);
	port (
		-- Users to add ports here
        adc_shdn    : out std_logic;
        adc_rd      : out std_logic;
        adc_convst  : out std_logic;
        adc_cs      : out std_logic;
        adc_busy    : in std_logic;
        adc_data    : in std_logic_vector(13 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic;

		-- Ports of Axi Master Bus Interface AXIS
		axis_aclk	: in std_logic;
		axis_aresetn	: in std_logic;
		axis_tvalid	: out std_logic;
		axis_tdata	: out std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
		axis_tstrb	: out std_logic_vector((C_AXIS_TDATA_WIDTH/8)-1 downto 0);
		axis_tlast	: out std_logic;
		axis_tready	: in std_logic
	);
end LTC1419_AXIS_v1_0;

architecture arch_imp of LTC1419_AXIS_v1_0 is
    type state is ( IDLE,                   
            INIT,
            CONV_START,
            CONV_WAIT,
            READ_DATA,
            DONE);      
                                                        
    signal capture_state    : state;
    signal go               : std_logic;
    signal cap_done             : std_logic;
    
    signal counter          : unsigned(6 downto 0);
    
    signal sampled_data     : std_logic_vector(13 downto 0);
begin

-- Instantiation of Axi Bus Interface S00_AXI
LTC1419_AXIS_v1_0_S00_AXI_inst : entity work.LTC1419_AXIS_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready,
		
		sampled_data      => sampled_data,
		go    => go,
		cap_done => cap_done
	);

-- Instantiation of Axi Bus Interface AXIS
LTC1419_AXIS_v1_0_AXIS_inst : entity work.LTC1419_AXIS_v1_0_AXIS
	generic map (
		C_M_AXIS_TDATA_WIDTH	=> C_AXIS_TDATA_WIDTH,
		C_M_START_COUNT	=> C_AXIS_START_COUNT
	)
	port map (
		M_AXIS_ACLK	=> axis_aclk,
		M_AXIS_ARESETN	=> axis_aresetn,
		M_AXIS_TVALID	=> axis_tvalid,
		M_AXIS_TDATA	=> axis_tdata,
		M_AXIS_TSTRB	=> axis_tstrb,
		M_AXIS_TLAST	=> axis_tlast,
		M_AXIS_TREADY	=> axis_tready,
		
		adc_data      => adc_data
		--adc_convst    => adc_convst
	);

	-- mode 1a of datasheet, data output always enabled
	-- CS_N and RD_N = 0
	adc_rd <= '0';
    adc_cs <= '0';
    adc_shdn <= '1';
    
    adc_convst <= '0' when (capture_state = CONV_START) else '1';
	
	-- tCONV max time = 1150 ns
    process(s00_axi_aclk)
    begin
    
    if (rising_edge (s00_axi_aclk)) then                                                       
        if(s00_axi_aresetn = '0') then                                                                                                              
            capture_state      <= IDLE; 
            counter <= to_unsigned(0, 7);                                                                                                                
        else                                                                                    
            case (capture_state) is                                                              
            when IDLE     =>
                counter <= to_unsigned(0, 7);                                                                    
                if (go = '1')
                then capture_state <= CONV_START;   
                else capture_state <= IDLE;      
                end if;                                                                                                              
            when CONV_START =>       
                counter <= counter + to_unsigned(1, 7);                                                                                               
                if (counter = to_unsigned(5, 7)) -- pulse the CONVST signal low for > 40ns
                then capture_state  <= CONV_WAIT;                                             
                else capture_state  <= CONV_START;                                              
                end if;
            when CONV_WAIT =>    
                counter <= counter + 1;                                                          
                if (counter = to_unsigned(125, 7)) -- wait for the ADC, tCONV max time is 1150ns
                then capture_state  <= READ_DATA;   
                else capture_state <= CONV_WAIT;      
                end if; 
            when READ_DATA =>
                sampled_data <= adc_data;                                                                 
                capture_state  <= DONE;   
            when DONE     =>                                                            
                if (go = '0') 
                then capture_state <= IDLE;   
                else capture_state <= DONE;      
                end if;                                                                                                                               
            when others    =>                                                                   
                capture_state <= IDLE;                                                                                                                                                    
            end case;                                                                             
        end if;                                                                                 
    end if;
    
                                 
    
    end process;
    
    cap_done <= '1' when (capture_state = DONE) else '0';    

end arch_imp;
