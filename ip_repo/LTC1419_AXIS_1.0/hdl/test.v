`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/12/2017 02:11:58 PM
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test(

    );
    
    reg clk;
    reg resetn;
    
    LTC1419_AXIS_v1_0 u0(
    .adc_shdn(),
    .adc_rd(),
    .adc_convst(),
    .adc_cs(),
    .adc_busy(),
    .adc_data(),

    .s00_axi_aclk(clk),
    .s00_axi_aresetn(),
    .s00_axi_awaddr(),
    .s00_axi_awprot(),
    .s00_axi_awvalid(),
    .s00_axi_awready(),
    .s00_axi_wdata(),
    .s00_axi_wstrb(),
    .s00_axi_wvalid(),
    .s00_axi_wready(),
    .s00_axi_bresp(),
    .s00_axi_bvalid(),
    .s00_axi_bready(),
    .s00_axi_araddr(),
    .s00_axi_arprot(),
    .s00_axi_arvalid(),
    .s00_axi_arready(),
    .s00_axi_rdata(),
    .s00_axi_rresp(),
    .s00_axi_rvalid(),
    .s00_axi_rready(),

    .axis_aclk(clk),
    .axis_aresetn(),
    .axis_tvalid(),
    .axis_tdata(),
    .axis_tstrb(),
    .axis_tlast(),
    .axis_tready()
    );
    
    initial begin
        clk = 0;
        resetn = 0;
        
        #5
        resetn = 1;
        
        #10
        go = 1;

    end
    
    always
        #1 clk = !clk;
    
endmodule
