-- Clk_Rst_Cnt.vhd
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--    Copyright(c) 2010 COM DEV Canada. All Rights Reserved.
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  TEMPLATE NUMBER     : 
--  PROJECT NAME        : SWSH
--  MODULE NAME         : Clk_Rst_Cnt.vhd
--  MODULE AUTHOR       : Rodney Norman
--  DATE CREATED        : 2010-December-18
--  MODULE PART NUMBER  : 
--  MODULE VERSION      : v00.01
--  MODULE LANGUAGE     : VHDL-93
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- |VERSION |AUTHOR  |DATE       |DESCRIPTION OF CHANGE
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  V00.01     RN     2010-12-18 Created.
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~+ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--
-- Prefixes (Start lower case): 
-- i_    = Inputs,
-- o_    = Outputs, 
-- io_   = Input/outputs
-- s_    = Signal
-- c_    = Combinatorial Signal
-- r_    = Registered Signal
-- k     = Constant
-- m_    = (Algorithmic) State Machine "State" signal
-- a_    = (Algorithmic) State Machine "State" (enumerated type)
-- t_    = Type
-- p_    = Process
-- v_    = variable
--
-- ?v_   = ? replaced by above letters, v = multi bit vector
--
-- Suffixes
-- _n    = Active low
--
--+ ~ ~ ~ ~ ~
library IEEE;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_STD.all;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- Entity declaration section start
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
entity Clk_Rst_Cnt is
--+ ~ ~ ~ ~ ~
-- Port name declarations
--+ ~ ~ ~ ~ ~
port   (
  Clk24M               : in    std_logic;
  nMASTER_RST          : in    std_logic;
  SW_Reset             :  In std_logic;

  Rst24M                : Out   std_logic;  -- s_Rst24M
  Clk6M                 : Out   std_logic;  -- s_Clk6M
  Rst6M                 : Out   std_logic;  -- s_Rst6M
                                          -- s_Tick167ns
                                          -- s_Clk1MCnt
  Tick1us              : out   std_logic;   -- s_Tick1us
  Tick1ms              : out   std_logic;   -- s_Tick1ms
  Tick1s               : out   std_logic    -- s_Tick1s
   );
End entity Clk_Rst_Cnt;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
architecture RTL of Clk_Rst_Cnt is


Signal s_Rst24M_0  :   std_logic;
Signal s_Rst24M    :   std_logic;
Signal s_Clk6MCnt  :  std_logic_vector(1 downto 0);
Signal s_Tick167ns :  std_logic;
Signal s_Clk1MCnt  :  std_logic_vector(2 downto 0);
Signal s_Tick1us   :   std_logic;
Signal s_Clk1KCnt  :  std_logic_vector(9 downto 0);
Signal s_Tick1ms   :   std_logic;
Signal s_Clk1HzCnt :  std_logic_vector(9 downto 0);
Signal s_Tick1s    :   std_logic;
Signal s_Rst6M_0   :   std_logic;
Signal s_Rst6M     :   std_logic;

Constant K0_10B    : std_logic_vector(9 downto 0) := (others => '0');
Constant K999_10B  : std_logic_vector(9 downto 0) := K0_10B + 999;

Begin -- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Rst24M :  Process (nMASTER_RST, Clk24M, SW_Reset)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
  If (nMASTER_RST = '0') Or (SW_Reset = '1')
  Then
    s_Rst24M_0 <= '1';
    s_Rst24M <= '1';
  ElsIf (rising_edge(Clk24M))
  Then
    s_Rst24M_0 <= '0';
    s_Rst24M <= s_Rst24M_0 ;
  End if;
End Process;

Rst24M <= s_Rst24M;


-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Clk6M :  Process (s_Rst24M, Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
  If (s_Rst24M = '1')
  Then  s_Clk6MCnt <= "00";
  ElsIf (rising_edge(Clk24M))    Then  s_Clk6MCnt <= s_Clk6MCnt + 1;
  End if;
End Process;

Clk6M <= s_Clk6MCnt(1);

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Rst6M :  Process (nMASTER_RST, s_Clk6MCnt(1), SW_Reset)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
  If (nMASTER_RST = '0')  Or (SW_Reset = '1')
  Then
    s_Rst6M_0 <= '1';
    s_Rst6M <= '1';
  ElsIf (rising_edge(s_Clk6MCnt(1)))
  Then
    s_Rst6M_0 <= '0';
    s_Rst6M <= s_Rst6M_0 ;
  End if;  
End Process;

Rst6M <= s_Rst6M;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Tick167ns :  Process (s_Rst24M, Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
  If (s_Rst24M = '1')
  Then s_Tick167ns <= '0';
  ElsIf (rising_edge(Clk24M)) 
  Then
    If (s_Clk6MCnt = "00")
    Then s_Tick167ns <= '1';
    Else s_Tick167ns <= '0';
    End if;
  End if;  
End Process;  

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Tick1us :  Process (s_Rst24M, Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
  If (s_Rst24M = '1')
  Then  
    s_Clk1MCnt <= "000";
    s_Tick1us  <= '0';
  ElsIf (rising_edge(Clk24M))    Then
    If (s_Tick167ns = '1')
    Then  
      If (s_Clk1MCnt = "101")
      Then
        s_Clk1MCnt <= "000";
        s_Tick1us  <= '1';
      Else
        s_Clk1MCnt <= s_Clk1MCnt + 1;
        s_Tick1us  <= '0';
      End if;
    Else
      s_Clk1MCnt <= s_Clk1MCnt;
      s_Tick1us  <= '0';
    End if;  
  End if;
End Process;

Tick1us <= s_Tick1us;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Tick1ms :  Process (s_Rst24M, Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
  If (s_Rst24M = '1')
  Then  
    s_Clk1KCnt <= K0_10B;
    s_Tick1ms  <= '0';
  ElsIf (rising_edge(Clk24M))    Then
    If (s_Tick1us = '1')
    Then
      If (s_Clk1KCnt = K999_10B)
      Then  
        s_Clk1KCnt <= K0_10B;
        s_Tick1ms  <= '1';
      Else
        s_Clk1KCnt <= s_Clk1KCnt + 1;
        s_Tick1ms  <= '0';
      End if;
    Else
      s_Clk1KCnt <= s_Clk1KCnt;
      s_Tick1ms  <= '0';
    End if;
  End if;
End Process;

Tick1ms <= s_Tick1ms;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
P_Tick1s :  Process (s_Rst24M, Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
  If (s_Rst24M = '1')
  Then
    s_Clk1HzCnt <= K0_10B;
    s_Tick1s  <= '0';
  ElsIf (rising_edge(Clk24M))    Then  
    If (s_Tick1ms = '1')
    Then
      If (s_Clk1HzCnt = K999_10B)
      Then  
        s_Clk1HzCnt <= K0_10B;
        s_Tick1s  <= '1';
      Else
        s_Clk1HzCnt <= s_Clk1HzCnt + 1;
        s_Tick1s  <= '0';
      End if;
    Else
      s_Clk1HzCnt <= s_Clk1HzCnt;
      s_Tick1s  <= '0';
    End if;
  End if;
End Process;

Tick1s <= s_Tick1s;

End architecture Rtl;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
