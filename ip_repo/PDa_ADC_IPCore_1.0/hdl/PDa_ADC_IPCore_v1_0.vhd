library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PDa_ADC_IPCore_v1_0 is
	generic (
		-- Users to add parameters here
        C_SAXI_DATA_WIDTH	: integer	:= 16;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S04_AXI
		C_S04_AXI_DATA_WIDTH	: integer	:= 32;
		C_S04_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
        --s04_CLK24M        : in    std_logic;
        Rst24M        : in    std_logic;
         
        VidIn         : In   std_logic_vector(13 downto 0);

        ConvSt_n      : Out   std_logic;

        --VidDt         : Out   std_logic_vector(13 downto 0);


        ClockL        : Out   std_logic;
        Cap_Gain      : Out   std_logic;
        LSync         : Out   std_logic;
		-- User ports ends
		

        axis_tdata      : out std_logic_vector(C_SAXI_DATA_WIDTH-1 downto 0); -- Transfer Data 
        axis_tvalid     : out std_logic; -- Transfer valid (required)
        axis_tready      : in std_logic;
        axis_tlast      : out std_logic;
		
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S04_AXI
		s04_axi_aclk	: in std_logic;
		s04_axi_aresetn	: in std_logic;
		s04_axi_awaddr	: in std_logic_vector(C_S04_AXI_ADDR_WIDTH-1 downto 0);
		s04_axi_awprot	: in std_logic_vector(2 downto 0);
		s04_axi_awvalid	: in std_logic;
		s04_axi_awready	: out std_logic;
		s04_axi_wdata	: in std_logic_vector(C_S04_AXI_DATA_WIDTH-1 downto 0);
		s04_axi_wstrb	: in std_logic_vector((C_S04_AXI_DATA_WIDTH/8)-1 downto 0);
		s04_axi_wvalid	: in std_logic;
		s04_axi_wready	: out std_logic;
		s04_axi_bresp	: out std_logic_vector(1 downto 0);
		s04_axi_bvalid	: out std_logic;
		s04_axi_bready	: in std_logic;
		s04_axi_araddr	: in std_logic_vector(C_S04_AXI_ADDR_WIDTH-1 downto 0);
		s04_axi_arprot	: in std_logic_vector(2 downto 0);
		s04_axi_arvalid	: in std_logic;
		s04_axi_arready	: out std_logic;
		s04_axi_rdata	: out std_logic_vector(C_S04_AXI_DATA_WIDTH-1 downto 0);
		s04_axi_rresp	: out std_logic_vector(1 downto 0);
		s04_axi_rvalid	: out std_logic;
		s04_axi_rready	: in std_logic
	);
end PDa_ADC_IPCore_v1_0;

architecture arch_imp of PDa_ADC_IPCore_v1_0 is

begin

-- Instantiation of Axi Bus Interface S04_AXI
PDa_ADC_IPCore_v1_0_S04_AXI_inst : entity work.PDa_ADC_IPCore_v1_0_S04_AXI
	generic map (
	    C_SAXI_DATA_WIDTH   => C_SAXI_DATA_WIDTH,
		C_S_AXI_DATA_WIDTH	=> C_S04_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S04_AXI_ADDR_WIDTH
	)
	port map (
	    S_CLK24M => s04_axi_aclk,
	    S_Rst24M => Rst24M,
	    S_VidIn => VidIn,
	    S_ConvSt_n => ConvSt_n,
	    axis_data => axis_tdata,
	    axis_ready => axis_tready, --
	    axis_valid => axis_tvalid, --
	    axis_last => axis_tlast,
	    S_ClockL => ClockL,
	    S_Cap_Gain => Cap_Gain,
	    S_LSync => LSync,
		S_AXI_ACLK	=> s04_axi_aclk,
		S_AXI_ARESETN	=> s04_axi_aresetn,
		S_AXI_AWADDR	=> s04_axi_awaddr,
		S_AXI_AWPROT	=> s04_axi_awprot,
		S_AXI_AWVALID	=> s04_axi_awvalid,
		S_AXI_AWREADY	=> s04_axi_awready,
		S_AXI_WDATA	=> s04_axi_wdata,
		S_AXI_WSTRB	=> s04_axi_wstrb,
		S_AXI_WVALID	=> s04_axi_wvalid,
		S_AXI_WREADY	=> s04_axi_wready,
		S_AXI_BRESP	=> s04_axi_bresp,
		S_AXI_BVALID	=> s04_axi_bvalid,
		S_AXI_BREADY	=> s04_axi_bready,
		S_AXI_ARADDR	=> s04_axi_araddr,
		S_AXI_ARPROT	=> s04_axi_arprot,
		S_AXI_ARVALID	=> s04_axi_arvalid,
		S_AXI_ARREADY	=> s04_axi_arready,
		S_AXI_RDATA	=> s04_axi_rdata,
		S_AXI_RRESP	=> s04_axi_rresp,
		S_AXI_RVALID	=> s04_axi_rvalid,
		S_AXI_RREADY	=> s04_axi_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
