-- Pda_Adc_Ctrl.vhd

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--    Copyright(c) 2010 COM DEV Canada. All Rights Reserved.
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  TEMPLATE NUMBER     : 
--  PROJECT NAME        : SWSH
--  MODULE NAME         : Adc_Ctrl.vhd
--  MODULE AUTHOR       : Rodney Norman  
--  DATE CREATED        : 2010-December-18
--  MODULE PART NUMBER  : 
--  MODULE VERSION      : v00.01
--  MODULE LANGUAGE     : VHDL-93
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~ + ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- |VERSION |AUTHOR  |DATE       |DESCRIPTION OF CHANGE
-- + ~ ~ ~ ~+ ~ ~ ~ ~+ ~ ~ ~ ~ ~ + ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--  V00.01     RN     2010-12-18 Created. 
library IEEE;
--use IEEE.std_logic_arith.all;
--use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_1164.all;

use IEEE.numeric_STD.all;

--use work.Swish_Pkg1.All;

--use IEEE.std_logic_textio.all;
--use std.textio.all;

Library UNISIM;
use UNISIM.vcomponents.all;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
entity Pda_Adc_Ctrl is
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
port   (

  i_Clk24M        : In    std_logic;
  i_Rst24M        : In    std_logic;

  --i_Cmnd_Reg      : In    Swish_Cmnd_Reg_Type;
  i_NFClkAdc      : in unsigned(11 downto 0); 
  i_NTDelayAdc    : in unsigned(6 downto 0);
  i_NTLsyncLo     : in unsigned(21 downto 0); 
  i_NTLsyncHi     : in unsigned(21 downto 0);
  i_Cap_Gain      : in std_logic;
  
  i_VidIn         : In   std_logic_vector(13 downto 0);

  o_ConvSt_n      : Out   std_logic;

  o_VidDt         : Out   std_logic_vector(13 downto 0);
  axis_valid        : out   std_logic;
  axis_ready        : in    std_logic;
  axis_aclk         : in    std_logic;
  axis_last         : out   std_logic;
  
  start             : in    std_logic;
  
  axi_aresetn       : in    std_logic;

  o_SlfTstRdAdd   : Out   std_logic_vector(7 downto 0);

  o_VidAdd        : Out   std_logic_vector(7 downto 0);
  o_VidWe         : Out   std_logic;

  o_ClockL        : Out   std_logic;
  o_Cap_Gain      : Out   std_logic;
  o_LSync         : Out   std_logic

     );
End entity Pda_Adc_Ctrl;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
architecture RTL of Pda_Adc_Ctrl is
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

Signal s_AdcClkDelEn     : std_logic;
Signal s_AdcClkCnt       : unsigned(11 downto 0);
Signal s_ErlyAdcClk      : std_logic;
Signal s_ErlyAdcClkTglEn : std_logic;
Signal s_AdcRegLdEn      : std_logic;

Signal s_AdcMemWrEnEn    : std_logic;

Signal s_ClockL          : std_logic;
Signal s_LSync           : std_logic;
Signal s_LSyncCnt        : unsigned(21 downto 0);
Signal s_LSyncCntZr      : std_logic;
Signal s_LSyncTglEn      : std_logic;
Signal s_PdaClkTglEn     : std_logic;
Signal s_LsyncFell       : std_logic;
Signal s_VidWeEn         : std_logic;
Signal s_VidAdd          : std_logic_vector(7 downto 0);
Signal s_LSyncCntEn      : std_logic;
Signal s_LSyncLoEn       : std_logic;

type state_type is (S_IDLE, S_RDY, S_XFER, S_END); 
signal stream_state, stream_next_state : state_type; 

signal counter          : unsigned(13 downto 0);
signal transfer_count   : unsigned(2 downto 0);

signal s_axis_valid     : std_logic;

signal s_fifo_wren      : std_logic;

signal fifo_out         : std_logic_vector(31 downto 0);
signal fifo_in          : std_logic_vector(31 downto 0);
signal fifo_full        : std_logic;
signal fifo_empty       : std_logic;
signal fifo_rden        : std_logic;
signal fifo_wren        : std_logic;

type fifo_reset_type is (S_IDLE, S_ENLO, S_RESET, S_ENLO2); 
signal fifo_reset_state, fifo_reset_next_state : fifo_reset_type;
signal fifo_reset       : std_logic;
signal fifo_reset_counter       : unsigned(2 downto 0);

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
-- Start of architecture code
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin


------------------
-- Generate the ADC clock

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_ErlyAdcClkTglEn : Process (s_AdcClkCnt, i_NFClkAdc)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_AdcClkCnt = unsigned(i_NFClkAdc))
Then s_ErlyAdcClkTglEn <= '1';
Else s_ErlyAdcClkTglEn <= '0';
End If;
End Process;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_AdcClkCnt : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then  s_AdcClkCnt <= to_unsigned(0,12);
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_ErlyAdcClkTglEn = '1'
  Then s_AdcClkCnt <= to_unsigned(0,12);
  Else s_AdcClkCnt <= s_AdcClkCnt + 1;
  End if;  
End if;  
End Process;  

------------------------------



-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_AdcClkDelEn : Process (s_AdcClkCnt, i_NTDelayAdc)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_AdcClkCnt = i_NTDelayAdc)
Then s_AdcClkDelEn      <= '1';
Else s_AdcClkDelEn      <= '0';
End If;
End Process;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_ConvSt_n : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then o_ConvSt_n <= '1';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_AdcClkDelEn = '1'
  Then o_ConvSt_n <= s_ErlyAdcClk; 
  End If;
End if;  
End Process; 

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_ErlyAdcClk : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then s_ErlyAdcClk <= '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_ErlyAdcClkTglEn = '1'
  Then s_ErlyAdcClk <= Not s_ErlyAdcClk; 
  End If;
End If;
End Process;



--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_AdcRegLdEn : Process (s_ErlyAdcClkTglEn, s_ErlyAdcClk)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_ErlyAdcClkTglEn = '1') And (s_ErlyAdcClk = '1')
Then s_AdcRegLdEn <= '1';
Else s_AdcRegLdEn <= '0';
End If;
End Process;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
s_PdaClkTglEn <= s_AdcRegLdEn;
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

s_fifo_wren <= s_AdcRegLdEn and not s_LSync;

---- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--p_VidDt : Process (i_Rst24M, i_Clk24M)
---- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
--Begin 
--If (i_Rst24M = '1') 
--Then o_VidDt <= K0_14b;
--Elsif (rising_edge(i_Clk24M)) 
--Then
--  If s_AdcRegLdEn = '1'
--  Then 
--    o_VidDt(13)          <= Not i_VidIn(13); 
--    o_VidDt(12 downto 0) <= i_VidIn(12 downto 0); 
--  End If;
--End if;  
--End Process; 

o_VidAdd      <= s_VidAdd;


--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_AdcMemWrEnEn : Process (s_AdcRegLdEn)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_AdcRegLdEn = '1')
Then s_AdcMemWrEnEn <= '1';
Else s_AdcMemWrEnEn <= '0';
End If;
End Process;

--FF_RCJKQ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LsyncFell: Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then  
  s_LsyncFell <=  '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_LSyncLoEn = '1'
  Then 
    If s_AdcMemWrEnEn = '1'
    Then  s_LsyncFell <= Not s_LsyncFell;
    Else  s_LsyncFell <= '1';
    End if;  
  Else 
    If s_AdcMemWrEnEn = '1'
    Then  s_LsyncFell <= '0';
    Else  s_LsyncFell <=  s_LsyncFell;
    End if;  
  End if;  
End If;  
End Process; 




-- AXI-Stream data out
Process (stream_state, axis_ready, start)
Begin
    Case stream_state is
        When S_IDLE =>
            If (start = '1') 
            Then stream_next_state <= S_RDY;
            Else stream_next_state <= S_IDLE;
            End if;
        When S_RDY =>
            If (axis_ready = '1') 
            Then stream_next_state <= S_XFER;
            Else stream_next_state <= S_RDY;
            End if;
        When S_XFER =>
            If (axis_ready = '0') 
            Then stream_next_state <= S_END;
            Else stream_next_state <= S_XFER;
            End if;
        When S_END =>
            If (start = '1')
            Then stream_next_state <= S_END;
            Else stream_next_state <= S_IDLE;
            End if;
        When others =>
            stream_next_state <= S_IDLE;
    End case;
End Process;

Process (axis_aclk)
Begin
If rising_edge(axis_aclk) then 
    If (axi_aresetn = '0')
    Then stream_state <= S_IDLE; 
    Else stream_state <= stream_next_state; 
    end if; 
End if; 
End Process;

Process (axis_aclk)
Begin
If rising_edge(axis_aclk) then 
    If (s_LSync = '1' and s_AdcRegLdEn = '1') 
    Then counter <= to_unsigned(0,14); 
    Else counter <= counter + to_unsigned(1,14); 
    end if; 
End if; 
End Process;


Process (axis_aclk)
Begin
If rising_edge(axis_aclk) then 
    If (s_axis_valid = '0') 
    Then transfer_count <= to_unsigned(7, 3);
    Elsif (s_axis_valid = '1' and axis_ready = '1')
    Then transfer_count <= transfer_count - to_unsigned(1, 3);
    end if; 
End if; 
End Process;


   -- FIFO18E1: 18Kb FIFO (First-In-First-Out) Block RAM Memory
   --           Artix-7
   -- Xilinx HDL Language Template, version 2016.3

   FIFO18E1_inst : FIFO18E1
   generic map (
      ALMOST_EMPTY_OFFSET => X"0080",   -- Sets the almost empty threshold
      ALMOST_FULL_OFFSET => X"0080",    -- Sets almost full threshold
      DATA_WIDTH => 18,                  -- Sets data width to 4-36
      DO_REG => 0,                      -- Enable output register (1-0) Must be 1 if EN_SYN = FALSE
      EN_SYN => TRUE,                  -- Specifies FIFO as dual-clock (FALSE) or Synchronous (TRUE)
      FIFO_MODE => "FIFO18",            -- Sets mode to FIFO18 or FIFO18_36
      FIRST_WORD_FALL_THROUGH => FALSE, -- Sets the FIFO FWFT to FALSE, TRUE
      INIT => X"000000000",             -- Initial values on output port
      SIM_DEVICE => "7SERIES",          -- Must be set to "7SERIES" for simulation behavior
      SRVAL => X"000000000"             -- Set/Reset value for output port
   )
   port map (
      -- Read Data: 32-bit (each) output: Read output data
      DO => fifo_out,                   -- 32-bit output: Data output
      DOP => open,                 -- 4-bit output: Parity data output
      -- Status: 1-bit (each) output: Flags and other FIFO status outputs
      ALMOSTEMPTY => open, -- 1-bit output: Almost empty flag
      ALMOSTFULL => open,   -- 1-bit output: Almost full flag
      EMPTY => fifo_empty,             -- 1-bit output: Empty flag
      FULL => fifo_full,               -- 1-bit output: Full flag
      RDCOUNT => open,         -- 12-bit output: Read count
      RDERR => open,             -- 1-bit output: Read error
      WRCOUNT => open,         -- 12-bit output: Write count
      WRERR => open,             -- 1-bit output: Write error
      -- Read Control Signals: 1-bit (each) input: Read clock, enable and reset input signals
      RDCLK => axis_aclk,             -- 1-bit input: Read clock
      RDEN => fifo_rden,               -- 1-bit input: Read enable
      REGCE => '1',             -- 1-bit input: Clock enable
      RST => fifo_reset,                 -- 1-bit input: Asynchronous Reset
      RSTREG => '0',           -- 1-bit input: Output register set/reset
      -- Write Control Signals: 1-bit (each) input: Write clock and enable input signals
      WRCLK => axis_aclk,             -- 1-bit input: Write clock
      WREN => fifo_wren,               -- 1-bit input: Write enable
      -- Write Data: 32-bit (each) input: Write input data
      DI => fifo_in,                   -- 32-bit input: Data input
      DIP => "0000"                  -- 4-bit input: Parity input
   );


-- argh! the fifo above is picky with its reset signal and rden / wren on startup

Process (fifo_reset_state, axi_aresetn, fifo_reset_counter)
Begin
    Case fifo_reset_state is
        When S_IDLE =>
            If (axi_aresetn = '0') 
            Then fifo_reset_next_state <= S_ENLO;
            Else fifo_reset_next_state <= S_IDLE;
            End if;
        When S_ENLO =>
            If (fifo_reset_counter = to_unsigned(7, 3)) 
            Then fifo_reset_next_state <= S_RESET;
            Else fifo_reset_next_state <= S_ENLO;
            End if;
        When S_RESET =>
            If (fifo_reset_counter = to_unsigned(7, 3)) 
            Then fifo_reset_next_state <= S_ENLO2;
            Else fifo_reset_next_state <= S_RESET;
            End if;
        When S_ENLO2 =>
            If (fifo_reset_counter = to_unsigned(7, 3)) 
            Then fifo_reset_next_state <= S_IDLE;
            Else fifo_reset_next_state <= S_ENLO2;
            End if;
        When others =>
            fifo_reset_next_state <= S_IDLE;
    End case;
End Process;

Process (axis_aclk)
Begin
If rising_edge(axis_aclk) then 
    If (axi_aresetn = '0') 
    Then 
        fifo_reset_state <= S_ENLO;
        fifo_reset_counter <= to_unsigned(0, 3);
    Else
        If (fifo_reset_state = S_IDLE)
        Then fifo_reset_counter <= to_unsigned(0, 3);
        Else fifo_reset_counter <= fifo_reset_counter + to_unsigned(1, 3);
        End if;
        
        fifo_reset_state <= fifo_reset_next_state; 
    end if; 
End if; 
End Process;


fifo_in <= "000000000000000000" & std_logic_vector(i_VidIn);
o_VidDt <= fifo_out(13 downto 0);

fifo_reset <= '1' when (fifo_reset_state = S_RESET) else '0';
fifo_wren <= s_fifo_wren when (fifo_full = '0' and fifo_reset_state = S_IDLE) else '0';
fifo_rden <= '1' when (stream_state = S_XFER and axis_ready = '1' and fifo_empty = '0' and fifo_reset_state = S_IDLE) else '0';

s_axis_valid <= '1' when (stream_state = S_RDY or stream_state = S_XFER) else '0';
axis_valid <= s_axis_valid;
axis_last <= '1' when transfer_count = to_unsigned(0, 3) else '0';

--

--FF_RCEDQ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_VidWeEn : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then s_VidWeEn <= '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_AdcMemWrEnEn = '1'
  Then s_VidWeEn <= s_LsyncFell; 
  End If;
End if;  
End Process; 

--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_VidWe : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then o_VidWe <= '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If (s_VidWeEn = '1') And (s_AdcRegLdEn = '1')
  Then o_VidWe <= '1';
  Else o_VidWe <= '0';
  End If;
End if;  
End Process;


--FF_RCEDQ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_ClockL : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then s_ClockL <= '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_PdaClkTglEn = '1'
  Then s_ClockL <= Not s_ClockL; 
  End If;
End if;  
End Process;

o_ClockL <= s_ClockL;

--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSyncCntEn : Process (s_PdaClkTglEn, s_ClockL)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_PdaClkTglEn = '1') And (s_ClockL = '0')
Then s_LSyncCntEn <= '1';
Else s_LSyncCntEn <= '0';
End If;
End Process;

--Eq ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSyncCntZr : Process (s_LSyncCnt)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_LSyncCnt = to_unsigned(0, 22))
Then s_LSyncCntZr      <= '1';
Else s_LSyncCntZr      <= '0';
End If;
End Process;

-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSyncCnt: Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then  s_LSyncCnt <= to_unsigned(0, 22);
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_LSyncCntEn = '1'
  Then 
    If s_LSyncCntZr = '1'
    Then 
      If s_LSync = '1'
      Then s_LSyncCnt <= unsigned(i_NTLsyncLo);
      Else s_LSyncCnt <= unsigned(i_NTLsyncHi);
      End if;
    Else s_LSyncCnt <= s_LSyncCnt - 1;
    End if;
  End if;
End if;  
End Process;  

--FF_RCEDQ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSync : Process (i_Rst24M, i_Clk24M)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin 
If (i_Rst24M = '1') 
Then s_LSync <= '0';
Elsif (rising_edge(i_Clk24M)) 
Then
  If s_LSyncTglEn = '1'
  Then s_LSync <= Not s_LSync; 
  End If;
End if;  
End Process; 

o_LSync <= s_LSync;

--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSyncTglEn : Process (s_LSyncCntEn, s_LSyncCntZr)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_LSyncCntEn = '1') And (s_LSyncCntZr = '1')
Then s_LSyncTglEn <= '1';
Else s_LSyncTglEn <= '0';
End If;
End Process;

--And  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
p_LSyncLoEn : Process (s_LSyncTglEn, s_LSync)
-- ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
Begin
If (s_LSyncTglEn = '1') And (s_LSync = '1')
Then s_LSyncLoEn <= '1';
Else s_LSyncLoEn <= '0';
End If;
End Process;

o_Cap_Gain <= i_Cap_Gain;


End architecture Rtl;
